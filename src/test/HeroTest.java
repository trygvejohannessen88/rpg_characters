package test;

import static org.junit.jupiter.api.Assertions.*;
import java.util.LinkedHashMap;
import org.junit.jupiter.api.Test;

import hero.Hero;
import hero.Mage;
import hero.Ranger;
import hero.Rogue;
import hero.Warrior;
import hero.attribute.Attribute;

class HeroTest {
	
	@Test
	void createCharacter_LevelIsOne() {
		Hero hero = new Warrior("warriorName");
		assertEquals(1,hero.getLevel());
	}
	
	@Test
	void characterGainsALevel_LevelIsTwo() {
		Hero hero = new Warrior("warriorName");
		hero.levelUp();
		assertEquals(2,hero.getLevel());
	}
	
	@Test
	void createWarrior_HasProperDefaultTotalAttributes() {
		Hero hero = new Warrior("name");
		LinkedHashMap<Attribute, Integer> attributes = hero.getTotalAttributes();
		assertEquals(5,attributes.get(Attribute.STR));
		assertEquals(2,attributes.get(Attribute.DEX));
		assertEquals(1,attributes.get(Attribute.INT));
	}
	
	@Test
	void levelUpWarrior_HasProperTotalAttributes() {
		Hero hero = new Warrior("name");
		hero.levelUp();
		LinkedHashMap<Attribute, Integer> attributes = hero.getTotalAttributes();
		assertEquals(8,attributes.get(Attribute.STR));
		assertEquals(4,attributes.get(Attribute.DEX));
		assertEquals(2,attributes.get(Attribute.INT));
	}
	
	@Test
	void createRanger_HasProperDefaultTotalAttributes() {
		Hero hero = new Ranger("name");
		LinkedHashMap<Attribute, Integer> attributes = hero.getTotalAttributes();
		assertEquals(1,attributes.get(Attribute.STR));
		assertEquals(7,attributes.get(Attribute.DEX));
		assertEquals(1,attributes.get(Attribute.INT));
	}
	
	@Test
	void levelUpRanger_HasProperTotalAttributes() {
		Hero hero = new Ranger("name");
		hero.levelUp();
		LinkedHashMap<Attribute, Integer> attributes = hero.getTotalAttributes();
		assertEquals(2,attributes.get(Attribute.STR));
		assertEquals(12,attributes.get(Attribute.DEX));
		assertEquals(2,attributes.get(Attribute.INT));
	}
	
	@Test
	void createRogue_HasProperDefaultTotalAttributes() {
		Hero hero = new Rogue("name");
		LinkedHashMap<Attribute, Integer> attributes = hero.getTotalAttributes();
		assertEquals(2,attributes.get(Attribute.STR));
		assertEquals(6,attributes.get(Attribute.DEX));
		assertEquals(1,attributes.get(Attribute.INT));
	}
	
	@Test
	void levelUpRogue_HasProperTotalAttributes() {
		Hero hero = new Rogue("name");
		hero.levelUp();
		LinkedHashMap<Attribute, Integer> attributes = hero.getTotalAttributes();
		assertEquals(3,attributes.get(Attribute.STR));
		assertEquals(10,attributes.get(Attribute.DEX));
		assertEquals(2,attributes.get(Attribute.INT));
	}
	
	@Test
	void createMage_HasProperDefaultTotalAttributes() {
		Hero hero = new Mage("name");
		LinkedHashMap<Attribute, Integer> attributes = hero.getTotalAttributes();
		assertEquals(1,attributes.get(Attribute.STR));
		assertEquals(1,attributes.get(Attribute.DEX));
		assertEquals(8,attributes.get(Attribute.INT));
	}
	
	@Test
	void levelUpMage_HasProperTotalAttributes() {
		Hero hero = new Mage("name");
		hero.levelUp();
		LinkedHashMap<Attribute, Integer> attributes = hero.getTotalAttributes();
		assertEquals(2,attributes.get(Attribute.STR));
		assertEquals(2,attributes.get(Attribute.DEX));
		assertEquals(13,attributes.get(Attribute.INT));
	}
}
