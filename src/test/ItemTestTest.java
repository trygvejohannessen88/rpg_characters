package test;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import hero.*;
import hero.item.Slot;
import hero.item.armour.*;
import hero.item.weapon.*;

class ItemTestTest {

	@Test
	void equip_WeaponWithTooHighLevelRequirement_ShouldThrowIllegalWeaponException() {
		//Arrange
		Hero hero = new Warrior("warriorName");
		Weapon axe = new Weapon("axe",2, Slot.WEAPON, WeaponType.AXE,9,1.5);
		String expected = "Cant equip weapon. You level is too low";
		//Act
		Exception exception = assertThrows(Exception.class, () -> {
			hero.equip(axe);
		});	
		String actual = exception.getMessage();
		//Assert
		assertEquals(expected,actual);
	}
	
	@Test
	void equip_ArmourWithTooHighLevelRequirement_ShouldThrowIllegalArmourException() {
		//Arrange
		Hero hero = new Warrior("warriorName");
		Armour armour = new Armour("armour",2, Slot.BODY, ArmourType.MAIL,2,1,1);
		String expected = "Cant equip armour. You level is too low";
		//Act
		Exception exception = assertThrows(Exception.class, () -> {
			hero.equip(armour);
		});	
		String actual = exception.getMessage();
		//Assert
		assertEquals(expected,actual);
	}
	
	@Test
	void equip_InvalidWeaponType_ShouldThrowIllegalWeaponException() {
		//Arrange
		Hero hero = new Warrior("warriorName");
		Weapon bow = new Weapon("bow",1, Slot.WEAPON, WeaponType.BOW,5,1.6);
		String expected = "Cant equip weapon. Your class cant equip this weapontype";
		//Act
		Exception exception = assertThrows(Exception.class, () -> {
			hero.equip(bow);
		});	
		String actual = exception.getMessage();
		//Assert
		assertEquals(expected,actual);
	}
	
	@Test
	void equip_InvalidArmourType_ShouldThrowIllegalWeaponException() {
		//Arrange
		Hero hero = new Warrior("warriorName");
		Armour pants = new Armour("pants",1, Slot.LEGS, ArmourType.CLOTH,2,1,1);	
		String expected = "Cant equip armour. Your class cant equip this armourtype";
		//Act
		Exception exception = assertThrows(Exception.class, () -> {
			hero.equip(pants);
		});	
		String actual = exception.getMessage();
		//Assert
		assertEquals(expected,actual);
	}
	
	@Test
	void equip_ValidWeapon_ShouldReturnTrue() throws IllegalWeaponException {
		Hero hero = new Warrior("warriorName");
		Weapon axe = new Weapon("axe",1, Slot.WEAPON, WeaponType.AXE,5,2);
		assertEquals(true,hero.equip(axe));
	}
	
	@Test
	void equip_ValidArmour_ShouldReturnTrue() throws IllegalArmourException {
		Hero hero = new Warrior("warriorName");
		Armour plateBodyArmour = new Armour("Plate Body Armour",1, Slot.HEAD, ArmourType.MAIL,1,0,0);
		assertEquals(true,hero.equip(plateBodyArmour));
	}
	
	@Test
	void calculateDPS_noWeaponIsEquiped_ShouldReturnCorrectValue() {
		Hero hero = new Warrior("warriorName");
		assertEquals(1.05,hero.getDPS());
	}
	
	@Test
	void calculateDPS_validWeaponIsEquiped_ShouldReturnCorrectValue() throws IllegalWeaponException {
		Hero hero = new Warrior("warriorName");
		Weapon axe = new Weapon("axe",1, Slot.WEAPON, WeaponType.AXE,5,2);
		hero.equip(axe);
		assertEquals(10.5,hero.getDPS());
	}
	
	@Test
	void calculateDPS_validWeaponAndArmourIsEquiped_ShouldReturnCorrectValue() throws Exception {
		Hero hero = new Warrior("warriorName");
		Weapon axe = new Weapon("axe",1, Slot.WEAPON, WeaponType.AXE,5,2);
		Armour armour = new Armour("armour",1, Slot.BODY, ArmourType.MAIL,5,0,0);
		hero.equip(axe);
		hero.equip(armour);
		assertEquals(11,hero.getDPS());
	}
}
