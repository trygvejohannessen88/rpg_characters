package hero;

import hero.attribute.Attribute;
import hero.item.armour.ArmourType;
import hero.item.weapon.WeaponType;

/**
* Mage is one of the classes that extends the base Hero class.
* It sets the Mage's mainAttribute and replaces his baseAttributes, 
* legalWeapontypes, legalArmourTypes and totalAttributes. 
* It overrides the levelUp method. 
* 
* @author      Trygve Johannessen
*/
public class Mage extends Hero {
	
	/**
	* The constructor overrides its parent constructor and sets the Mages name based on the parameter.
	* It sets default values for baseAttributes, totalAttributes, items, leagalWeaponTypes, leagalArmourTypes
	*
	* @param name     #String value that represents the Hero's name
	*/
	public Mage(String name) {
		super(name);
		mainAttribute = Attribute.INT;
		baseAttributes.replace(Attribute.STR,1);
		baseAttributes.replace(Attribute.DEX,1);
		baseAttributes.replace(Attribute.INT,8);
		leagalWeaponTypes.replace(WeaponType.STAFF,true);
		leagalWeaponTypes.replace(WeaponType.WAND,true);
		leagalArmourTypes.replace(ArmourType.CLOTH,true);
		updateTotalAttributes();
		maxHP = 60;
		HP = 60;
		attackMessage = "Fireball";
	}
	
	/**
	* Overrides the parent levelUp method.
	*/
	@Override
	public void levelUp() {
		level++;
		baseAttributes.replace(Attribute.STR,baseAttributes.get(Attribute.STR) + 1);
		baseAttributes.replace(Attribute.DEX,baseAttributes.get(Attribute.DEX) + 1);
		baseAttributes.replace(Attribute.INT,baseAttributes.get(Attribute.INT) + 5);
		updateTotalAttributes();	
		maxHP += 6;
		HP = maxHP;
	}

}
