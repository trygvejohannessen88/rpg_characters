package hero;

import hero.attribute.Attribute;
import hero.item.armour.ArmourType;
import hero.item.weapon.WeaponType;

/**
* Warrior is one of the classes that extends the base Hero class.
* It sets the Mage's mainAttribute and replaces his baseAttributes, 
* legalWeapontypes, legalArmourTypes and totalAttributes. 
* It overrides the levelUp method. 
* 
* @author      Trygve Johannessen
*/
public class Warrior extends Hero {
	
	/**
	* The constructor overrides its parent constructor and sets the Mages name based on the parameter.
	* It sets default values for baseAttributes, totalAttributes, items, leagalWeaponTypes, leagalArmourTypes
	*
	* @param name     #String value that represents the Hero's name
	*/
	public Warrior(String name) {
		super(name);
		mainAttribute = Attribute.STR;
		baseAttributes.replace(Attribute.STR,5);
		baseAttributes.replace(Attribute.DEX,2);
		baseAttributes.replace(Attribute.INT,1);
		leagalWeaponTypes.replace(WeaponType.SWORD,true);
		leagalWeaponTypes.replace(WeaponType.HAMMER,true);
		leagalWeaponTypes.replace(WeaponType.AXE,true);
		leagalArmourTypes.replace(ArmourType.MAIL,true);
		leagalArmourTypes.replace(ArmourType.PLATE,true);
		updateTotalAttributes();
		maxHP = 100;
		HP = 100;
		attackMessage = "Stab";
	}
	
	/**
	* Overrides the parent levelUp method.
	*/
	@Override
	public void levelUp() {
		level++;
		baseAttributes.replace(Attribute.STR,baseAttributes.get(Attribute.STR) + 3);
		baseAttributes.replace(Attribute.DEX,baseAttributes.get(Attribute.DEX) + 2);
		baseAttributes.replace(Attribute.INT,baseAttributes.get(Attribute.INT) + 1);
		updateTotalAttributes();
		maxHP += 10;
		HP = maxHP;
	}
}