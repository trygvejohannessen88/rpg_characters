package hero;

import hero.attribute.Attribute;
import hero.item.*;
import hero.item.weapon.*;

/**
* Monster is one of the classes that extends the base Hero class.
* This is an experimental class only used in the experimental Adventure class 
* 
* @author      Trygve Johannessen
*/
public class Monster extends Hero {
	
	public Monster(String name, int str, int HP) throws IllegalWeaponException {
		super(name);
		mainAttribute = Attribute.STR;
		baseAttributes.replace(Attribute.STR,str);
		updateTotalAttributes();
		maxHP = HP;
		this.HP = HP;
		leagalWeaponTypes.replace(WeaponType.SWORD,true);
		equip(new Weapon("sword",1, Slot.WEAPON, WeaponType.SWORD,5,1.1));
	}

	@Override
	public void levelUp() {
		level++;
		baseAttributes.replace(Attribute.STR,baseAttributes.get(Attribute.STR) + 3);
		baseAttributes.replace(Attribute.DEX,baseAttributes.get(Attribute.DEX) + 2);
		baseAttributes.replace(Attribute.INT,baseAttributes.get(Attribute.INT) + 1);
		updateTotalAttributes();
		maxHP += 10;
		HP = maxHP;
	}

}
