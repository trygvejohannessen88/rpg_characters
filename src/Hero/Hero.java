package hero;

import java.util.*;
import java.util.Map.Entry;
import doesDPS.doesDPS;
import hero.attribute.Attribute;
import hero.item.Item;
import hero.item.Slot;
import hero.item.weapon.*;
import hero.item.armour.*;

/**
* Hero is a abstract base class that gets extended by the character classes (Warrior, Mage, Rogue, Ranger).
* The hero class holds information about the Hero's name, level, mainAttribute, 
* baseAttributes, totalAttributes, maxHP, HP, attackMessage, equipped items, legalWeaponTypes and legalArmourTypes.
* This class also has the abstract method levelUp() for leveling up the Hero that gets overloaded in its subclass.
* And it has methods for equipping Weapons and Armour.
* It has a helper method updateTotalAttributes() that is executed when the character levels up or equips an item.
* It has a method getDPS() that calculates the Heroes's DPS based on the equipped Weapon and the characters mainAttibute.
* 
* @author      Trygve Johannessen
*/
public abstract class Hero implements doesDPS {
	protected String name;
	protected int level = 1;
	protected Attribute mainAttribute;
	protected LinkedHashMap<Attribute, Integer> baseAttributes = new LinkedHashMap<>();
	protected LinkedHashMap<Attribute, Integer> totalAttributes = new LinkedHashMap<>();
	protected int maxHP;
	protected int HP;
	protected String attackMessage = "attack";
	protected LinkedHashMap<Slot, Item> items = new LinkedHashMap<>();
	protected LeagalWeaponTypes leagalWeaponTypes = new LeagalWeaponTypes();
	protected LeagalArmourTypes leagalArmourTypes = new LeagalArmourTypes();
	
	/**
	* The constructor takes the Hero's name as a parameter and sets default values for 
	* baseAttributes, totalAttributes, items, leagalWeaponTypes, leagalArmourTypes
	*
	* @param name     #String value that represents the Hero's name
	*/
	public Hero(String name) {
		this.name = name;
		baseAttributes.put(Attribute.STR,1);
		baseAttributes.put(Attribute.DEX,1);
		baseAttributes.put(Attribute.INT,1);
		totalAttributes.put(Attribute.STR,1);
		totalAttributes.put(Attribute.DEX,1);
		totalAttributes.put(Attribute.INT,1);
		items.put(Slot.HEAD,null);
		items.put(Slot.BODY,null);
		items.put(Slot.LEGS,null);
		items.put(Slot.WEAPON,null);
	}
	
	/**
	* Abstract method for leveling up the Hero. 
	* Gets overridden in the subclasses (Warrior, Mage, Rogue, Ranger).
	*/
	public abstract void levelUp();
	
	/**
	* This method takes a Weapon-object and tries to equip it in the character appropriate item slot.
	* The method throws an IllegalWeaponException if the weapon has an illegal type or the required level is to high.
	*
	* @param weapon    	Weapon-object that the character is trying to equip
	* @return success	boolean value is returned true if the item was equipped successfully
	*/
	public boolean equip(Weapon weapon) throws IllegalWeaponException {	
		WeaponType weapontype = weapon.getWeaponType();
		boolean isLegal = leagalWeaponTypes.get(weapontype);
		if (!isLegal) {
			throw new IllegalWeaponException("Cant equip weapon. Your class cant equip this weapontype");
		}
		if (level<weapon.getRequiredLevel()) {
			throw new IllegalWeaponException("Cant equip weapon. You level is too low");
		}
		items.replace(weapon.getSlot(),weapon);		
		return true;
	}
	
	/**
	* This method takes a Armour-object and tries to equip it in the character appropriate item slot.
	* The method throws an IllegalArmourException if the Armour has an illegal type or the required level is to high.
	*
	* @param armour    	Armour-object that the character is trying to equip
	* @return success	boolean value is returned true if the item was equipped successfully
	*/
	public boolean equip(Armour armour) throws IllegalArmourException {
		ArmourType armourType = armour.getArmourType();
		boolean isLegal = leagalArmourTypes.get(armourType);
		if (!isLegal) {
			throw new IllegalArmourException("Cant equip armour. Your class cant equip this armourtype");
		}
		if (level<armour.getRequiredLevel()) {
			throw new IllegalArmourException("Cant equip armour. You level is too low");
		}
		items.replace(armour.getSlot(),armour);
		updateTotalAttributes();
		return true;	
	}
	
	/**
	* This is a helper-method that updates the Hero's total attributes.
	* It is executed every time the hero equips an item or levels up.
	* A Heroes total attribute is the sum of his basic attribute and bonuses from equipped items.
	*/
	public void updateTotalAttributes() {
		for (Entry<Attribute,Integer> entry: baseAttributes.entrySet()) {
			Attribute attribute = entry.getKey();
			Armour armour;
			int newValue = baseAttributes.get(attribute);
			armour = (Armour) items.get(Slot.HEAD);
			if (armour != null) newValue += armour.getAttributes().get(attribute); 
			armour = (Armour) items.get(Slot.BODY);
			if (armour != null) newValue += armour.getAttributes().get(attribute); 
			armour = (Armour) items.get(Slot.LEGS);
			if (armour != null) newValue += armour.getAttributes().get(attribute);
			totalAttributes.replace(attribute,newValue);
		}
	}
	
	public LinkedHashMap<Attribute, Integer> getTotalAttributes() {
		return totalAttributes;	
	}
	
	/**
	* This method returns how much DPS the Hero does.
	* It calculates the Heroes's DPS based on the equipped Weapon and the characters mainAttibute.
	*
	* @return dps	double value that represents how much DPS(damage per second) the Hero does 
	*/
	@Override
	public double getDPS() {
		Weapon weapon = (Weapon) items.get(Slot.WEAPON);
		if (weapon != null) {
			return weapon.getDPS() * (1.0 + totalAttributes.get(mainAttribute)/100.0);		
		}
		return 1.0 * (1.0 + totalAttributes.get(mainAttribute)/100.0);
	}
	
	public String getName() {
		return name;
	}
	
	public int getLevel() {
		return level;
	}
	
	public int getHP() {
		return HP;
	}
	
	public void looseHP(int hP) {
		HP -= hP;
	}

	public String getAttackMessage() {
		return attackMessage;
	}
	
	@Override
	public String toString() {
		String str = name + ", lvl=" + level;
		for(Entry<Attribute, Integer> entry: baseAttributes.entrySet()) {
			str += " " + entry;
		}
		for(Entry<Slot, Item> entry: items.entrySet()) {
			str += "\n" + entry;
		}
		str += "\n" + "TOTAL=";
		for(Entry<Attribute, Integer> entry: totalAttributes.entrySet()) {
			str += " " + entry;
		}
		str += "\n" + "maxHP= " + maxHP;
		str += "\n" + "HP= " + HP;
		return str;
	}

}
