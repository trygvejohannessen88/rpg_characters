package hero;

import hero.attribute.Attribute;
import hero.item.armour.ArmourType;
import hero.item.weapon.WeaponType;

/**
* Rogue is one of the classes that extends the base Hero class.
* It sets the Mage's mainAttribute and replaces his baseAttributes, 
* legalWeapontypes, legalArmourTypes and totalAttributes. 
* It overrides the levelUp method. 
* 
* @author      Trygve Johannessen
*/
public class Rogue extends Hero {
	
	/**
	* The constructor overrides its parent constructor and sets the Mages name based on the parameter.
	* It sets default values for baseAttributes, totalAttributes, items, leagalWeaponTypes, leagalArmourTypes
	*
	* @param name     #String value that represents the Hero's name
	*/
	public Rogue(String name) {
		super(name);
		mainAttribute = Attribute.DEX;
		baseAttributes.replace(Attribute.STR,2);
		baseAttributes.replace(Attribute.DEX,6);
		baseAttributes.replace(Attribute.INT,1);
		leagalWeaponTypes.replace(WeaponType.SWORD,true);
		leagalWeaponTypes.replace(WeaponType.DAGGER,true);
		leagalArmourTypes.replace(ArmourType.LEATHER,true);
		leagalArmourTypes.replace(ArmourType.MAIL,true);
		updateTotalAttributes();
		maxHP = 80;
		HP = 80;
		attackMessage = "sneaky stab";
	}
	
	/**
	* Overrides the parent levelUp method.
	*/
	@Override
	public void levelUp() {
		level++;
		baseAttributes.replace(Attribute.STR,baseAttributes.get(Attribute.STR) + 1);
		baseAttributes.replace(Attribute.DEX,baseAttributes.get(Attribute.DEX) + 4);
		baseAttributes.replace(Attribute.INT,baseAttributes.get(Attribute.INT) + 1);
		updateTotalAttributes();
		maxHP += 8;
		HP = maxHP;
	}

}
