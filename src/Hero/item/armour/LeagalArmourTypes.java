package hero.item.armour;

import java.util.HashMap;

@SuppressWarnings("serial")
public class LeagalArmourTypes extends HashMap<ArmourType, Boolean> {
	
	public LeagalArmourTypes() {
		put(ArmourType.CLOTH,false);
		put(ArmourType.LEATHER,false);
		put(ArmourType.MAIL,false);
		put(ArmourType.PLATE,false);
	}
}

