package hero.item.armour;

/**
* Custom Exception class that gets thrown when a Hero tries to equip an illegal Armour
* 
* @author      Trygve Johannessen
*/
@SuppressWarnings("serial")
public class IllegalArmourException extends Exception {
	
	public IllegalArmourException(String message) {	
		super(message);
	}
	
}
