package hero.item.armour;

import java.util.HashMap;
import java.util.Map.Entry;
import hero.attribute.*;
import hero.item.Item;
import hero.item.Slot;

/**
* The Armour class extends the abstract Item class
* It has an armourType and a HashMap of attribute values
* 
* @author      Trygve Johannessen
*/
public class Armour extends Item {
	private ArmourType armourType;
	private HashMap<Attribute,Integer> attributes = new HashMap<>();
	
	/**
	* The constructor takes the Armor's name, requiredLevel, slot, 
	* armourType, strength, dexterity and intelligence as parameters.
	*
	* @param name     #String value that represents the Hero's name
	*/
	public Armour(String name, int requiredLevel, Slot slot, ArmourType armourType, int str, int dex, int inte) {
		super(name, requiredLevel, slot);
		this.armourType = armourType;
  		attributes.put(Attribute.STR, str);
  		attributes.put(Attribute.DEX, dex);
  		attributes.put(Attribute.INT, inte);
	}
	
	public ArmourType getArmourType() {
		return armourType;
	}
	
	public HashMap<Attribute, Integer> getAttributes() {
		return attributes;
	}

	@Override
	public String toString() {
		String str = " lvl=" + requiredLevel + ", " + getSlot() + ", " + armourType + ", " + name;
		for(Entry<Attribute, Integer> entry: attributes.entrySet()) {
			str += " " + entry;
		}
		return str;
	}
	
}
