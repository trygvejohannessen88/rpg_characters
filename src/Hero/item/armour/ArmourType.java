package hero.item.armour;

/**
* Enum class that represents a Armour-object's ArmourTpe.
* 
* @author      Trygve Johannessen
*/
public enum ArmourType {
	CLOTH, LEATHER, MAIL, PLATE
}
