package hero.item;

/**
* Enum class that represents a Item-object's ItemSlot.
* All heroes have HashMap with one of each of these values and a corresponding item.
* 
* @author      Trygve Johannessen
*/
public enum Slot {
	HEAD, BODY, LEGS, WEAPON;
}
