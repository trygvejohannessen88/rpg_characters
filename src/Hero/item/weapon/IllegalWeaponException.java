package hero.item.weapon;

/**
* Custom Exception class that gets thrown when a Hero tries to equip an illegal Weapon
* 
* @author      Trygve Johannessen
*/
@SuppressWarnings("serial")
public class IllegalWeaponException extends Exception {
	
	public IllegalWeaponException(String message) {	
		super(message);
	}
	
}
