package hero.item.weapon;

import doesDPS.doesDPS;
import hero.item.*;

/**
* The Weapon class extends the abstract Item class
* It has a weaponType and values for damage and attackSpeed.
* 
* @author      Trygve Johannessen
*/
public class Weapon extends Item implements doesDPS {
	private WeaponType weaponType;
	private double damage;
	private double attackSpeed;
	
	/**
	* The constructor takes the Weapons's name, requiredLevel, slot, 
	* weaponType, damage and attackSpeed.
	*
	* @param name     #String value that represents the Hero's name
	*/
	public Weapon(String name, int requiredLevel, Slot slot, WeaponType weaponType, double damage, double attackSpeed) {
		super(name, requiredLevel, slot);
		this.weaponType = weaponType;
		this.damage = damage;
		this.attackSpeed = attackSpeed;
	}
	
	/**
	* This method returns the Weapons DPS- value.
	*
	* @param dps     #double value represents the Weapons DPS- value.
	*/
	@Override
	public double getDPS() {
		return damage * attackSpeed;
	}
	
	public WeaponType getWeaponType() {
		return weaponType;
	}

	@Override
	public String toString() {
		return " lvl=" + requiredLevel + ", " + getSlot() + ", " + weaponType 
				+ ", damage=" + damage + ", speed=" + attackSpeed + ", DPS=" + getDPS() + ", " + name;
	}

}
