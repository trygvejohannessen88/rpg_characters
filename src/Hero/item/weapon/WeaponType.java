package hero.item.weapon;
/**
* Enum class that represents a Weapon-object's WeaponTpe.
* 
* @author      Trygve Johannessen
*/
public enum WeaponType {
	BOW,DAGGER,SWORD,HAMMER,AXE,STAFF,WAND
}
