package hero.item.weapon;

import java.util.HashMap;

@SuppressWarnings("serial")
public class LeagalWeaponTypes extends HashMap<WeaponType, Boolean> {
	
	public LeagalWeaponTypes() {
		put(WeaponType.BOW,false);
		put(WeaponType.DAGGER,false);
		put(WeaponType.SWORD,false);
		put(WeaponType.HAMMER,false);
		put(WeaponType.AXE,false);
		put(WeaponType.STAFF,false);
		put(WeaponType.WAND,false);
	}
}
