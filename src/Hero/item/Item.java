package hero.item;

/**
* Abstract base class that represents an Item that a Hero can equip.
* It gets extended by the Weapon and Armour- classes.
* 
* @author      Trygve Johannessen
*/
public abstract class Item {
	protected String name = "untitled";
	protected int requiredLevel = 1;
	protected Slot slot;
	
	/**
	* The constructor takes the Item's name, requiredLevel and slot as parameters.
	*
	* @param name     #String value that represents the Hero's name
	*/
	public Item(String name, int requiredLevel, Slot slot) {
		this.name = name;
		this.requiredLevel = requiredLevel;
		this.slot = slot;
	}
	
	public String getName() {
		return name;
	}
	
	public int getRequiredLevel() {
		return requiredLevel;
	}

	public Slot getSlot() {
		return slot;
	}
	
}
