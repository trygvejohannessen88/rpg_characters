package hero.attribute;

/**
* Enum class that represents an Attribute value. All Heroes have the attributes STR, DEX and INT.
* Armour-object also has these attributes. They get added to a hero's totalAttribute when he equips the items.  
* 
* @author      Trygve Johannessen
*/
public enum Attribute {
	STR, DEX, INT
}
