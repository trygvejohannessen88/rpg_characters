package hero;

import hero.attribute.Attribute;
import hero.item.armour.ArmourType;
import hero.item.weapon.WeaponType;

/**
* Ranger is one of the classes that extends the base Hero class.
* It sets the Mage's mainAttribute and replaces his baseAttributes, 
* legalWeapontypes, legalArmourTypes and totalAttributes. 
* It overrides the levelUp method. 
* 
* @author      Trygve Johannessen
*/
public class Ranger extends Hero {
	
	/**
	* The constructor overrides its parent constructor and sets the Mages name based on the parameter.
	* It sets default values for baseAttributes, totalAttributes, items, leagalWeaponTypes, leagalArmourTypes
	*
	* @param name     #String value that represents the Hero's name
	*/
	public Ranger(String name) {
		super(name);
		mainAttribute = Attribute.DEX;
		baseAttributes.replace(Attribute.STR,1);
		baseAttributes.replace(Attribute.DEX,7);
		baseAttributes.replace(Attribute.INT,1);
		leagalWeaponTypes.replace(WeaponType.BOW,true);
		leagalArmourTypes.replace(ArmourType.LEATHER,true);
		leagalArmourTypes.replace(ArmourType.MAIL,true);
		updateTotalAttributes();
		maxHP = 70;
		HP = 70;
		attackMessage = "careful shot";
	}
	
	/**
	* Overrides the parent levelUp method.
	*/
	@Override
	public void levelUp() {
		level++;
		baseAttributes.replace(Attribute.STR,baseAttributes.get(Attribute.STR) + 1);
		baseAttributes.replace(Attribute.DEX,baseAttributes.get(Attribute.DEX) + 5);
		baseAttributes.replace(Attribute.INT,baseAttributes.get(Attribute.INT) + 1);
		updateTotalAttributes();
		maxHP += 7;
		HP = maxHP;
	}

}
