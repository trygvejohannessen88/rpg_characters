package adventure;

import java.util.ArrayList;
import java.util.Scanner;
import hero.*;
import hero.item.Slot;
import hero.item.armour.*;
import hero.item.weapon.*;

/**
* This is an experimental class with some additional 
* features other than those required in the assignment
* It makes a very simple game out of the java project
* 
* @author      Trygve Johannessen
*/
public class Adventure {
	static ArrayList<Hero> heroes = new ArrayList<>();
	static ArrayList<Hero> monsters = new ArrayList<>();
	static Hero hero;
	
	public static void main(String[] args) throws Exception {
		init();
		selectHero();
		
		Scanner scanner = new Scanner(System.in);
		String input = "";
		
		while (monsters.size()>0 && hero.getHP()>0 && input != "n") {
			System.out.println("Continue? (y/n)");
			input = scanner.next();
			if (input != "n") fight(hero,pickMonster());	
			if (monsters.size()==0) System.out.println("Congratulations! You have won!");
			if (hero.getHP()<=0) System.out.println("Game over");
		}
		scanner.close();
	}
	
	private static void selectHero() {
		Scanner scanner = new Scanner(System.in);
		String input = "";
		while (!input.matches("[1-4]")) {
			System.out.println("Please select Hero. Warrior(1), Rogue(2), Ranger(3), Mage(4)");
			input = scanner.next();
		}
		hero = heroes.get(Integer.parseInt(input)-1);
	}
	
	private static Hero pickMonster() {
		int numb = (int) (Math.random()*monsters.size());
		Hero monster = monsters.get(numb);
		monsters.remove(numb);
		return monster;
	}
	
	public static void fight(Hero hero, Hero monster) {
		while (hero.getHP()>0 && monster.getHP()>0) {
			hero.looseHP((int)monster.getDPS());	
			monster.looseHP((int)hero.getDPS());
			System.out.print(hero.getName() + " uses " + hero.getAttackMessage() + " to attack ");
			System.out.print(monster.getName() + " for " + (int)hero.getDPS() + " dmg. "); 
			System.out.println(monster.getName() + " has " + monster.getHP() + " HP left");
			System.out.print(monster.getName() + " attacks " + hero.getName() + " for ");
			System.out.println((int)monster.getDPS() + " dmg. " + hero.getName() + " has " + hero.getHP() + " HP left");
		}
		if (hero.getHP()<=0) {
			System.out.println(hero.getName() + " is dead " + "\n");
		}
		else {
			System.out.println(monster.getName() + " is dead " + "\n");
			hero.levelUp();
			System.out.print(hero.getName() + " the " + hero.getClass().getSimpleName());
			System.out.println(" is now lvl " + hero.getLevel());
		}
	}
	
	private static void init() throws Exception {
		monsters.add(new Monster("Skeleton",6,60));
		monsters.add(new Monster("Zombie",7,80));
		monsters.add(new Monster("Banshee",9,50));
		monsters.add(new Monster("Vampire",9,90));
		monsters.add(new Monster("Dread Knight",10,100));
		monsters.add(new Monster("Harpy",7,50));
		monsters.add(new Monster("Dragon",12,120));
		
		hero = new Warrior("Einar");
		hero.equip(new Armour("mail helmet",1, Slot.HEAD, ArmourType.MAIL,14,0,0));
		hero.equip(new Armour("mail armour",1, Slot.BODY, ArmourType.MAIL,18,0,0));
		hero.equip(new Armour("mail pants",1, Slot.LEGS, ArmourType.MAIL,16,0,0));
		hero.equip(new Weapon("sword",1, Slot.WEAPON, WeaponType.SWORD,5,1.1));
		heroes.add(hero);
		
		hero = new Rogue("John");
		hero.equip(new Armour("leather cap",1, Slot.HEAD, ArmourType.LEATHER,0,16,0));
		hero.equip(new Armour("leather armour",1, Slot.BODY, ArmourType.LEATHER,0,20,0));
		hero.equip(new Armour("leather pants",1, Slot.LEGS, ArmourType.LEATHER,0,18,0));
		hero.equip(new Weapon("dagger",1, Slot.WEAPON, WeaponType.DAGGER,3,1.5));
		heroes.add(hero);
		
		hero = new Ranger("Pete");
		hero.equip(new Armour("leather cap",1, Slot.HEAD, ArmourType.LEATHER,0,16,0));
		hero.equip(new Armour("leather armour",1, Slot.BODY, ArmourType.LEATHER,0,20,0));
		hero.equip(new Armour("leather pants",1, Slot.LEGS, ArmourType.LEATHER,0,18,0));
		hero.equip(new Weapon("dagger",1, Slot.WEAPON, WeaponType.BOW,6,1.3));
		heroes.add(hero);
		
		hero = new Mage("Timothy");
		hero.equip(new Armour("cloth hood",1, Slot.HEAD, ArmourType.CLOTH,0,0,20));
		hero.equip(new Armour("cloth robe",1, Slot.BODY, ArmourType.CLOTH,0,0,24));
		hero.equip(new Armour("cloth pants",1, Slot.LEGS, ArmourType.CLOTH,0,0,22));
		hero.equip(new Weapon("staff",1, Slot.WEAPON, WeaponType.STAFF,10,0.8));
		heroes.add(hero);
	}

}
