package doesDPS;

/**
* The doesDPS interface has a method getDPS
* It is implemented in thw weapon class and the Hero class
* 
* @author      Trygve Johannessen
*/
public interface doesDPS {
	
	public double getDPS();

}
